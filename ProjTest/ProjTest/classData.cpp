#include "classData.h"

void Data::addOutcome(int num, string title) {
	//newOutcome creates a new pointer to store the data
	outcome * newOutcome = new outcome;
	newOutcome->next = nullptr;
	newOutcome->id = num;
	newOutcome->title = title;
	//the if/else block finds the end of the linked list and appends the data in
	//newOutcome to the end of it
	if (head == nullptr) {
		head = newOutcome;
	}
	else {
		//the "current" pointer finds the end of the list, and sets it's "next" pointer
		//to the location of the new outcome node
		outcome* current = head;
		while (current->next != nullptr) {
			current = current->next;
		}
		current->next = newOutcome;
	}
}

void Data::addStudent(int num) {
	//newStudent creates a new pointer to store the data
	students * newStudent = new students;
	newStudent->next = nullptr;
	newStudent->num = num;
	//the if/else block finds the end of the linked list and appends the data in
	//newOutcome to the end of it
	if (headStudent == nullptr) {
		headStudent = newStudent;
	}
	else {
		//the "current" pointer finds the end of the list, and sets it's "next" pointer
		//to the location of the new student node
		students* current = headStudent;
		while (current->next != nullptr) {
			current = current->next;
		}
		current->next = newStudent;
	}
}

void Data::giveScore(int stuID, double id, double inScore) {
	//the search pointer is used to check the IDs of the nodes in the list
	students* search = headStudent;
	//if the process ran successfully and the student was found, the bool will be set to 1
	//if it failed to locate the student, an error message will show
	bool success = 0;
	//this will search through the entire linked list for the student, stopping once it
	//finds them, or it reaches the end of the list
	for (search != headStudent; search != nullptr; search = search->next) {
		//if the search pointer matches the ID of the student we're looking for, it will run the command
		//to add a score to their "profile"
		if (search->num == stuID) {
			search->addScore(id, inScore);
			success = 1;
			break;
		}
	}
	if (success == 0) {
		cout << "Error, could not find student.";
	}
}

void Data::students::addScore(double id, double inScore) {
	//this pointer will hold the data that will be placed at the end of the linked list
	score * newScore = new score;
	newScore->next = nullptr;
	newScore->outcomeID = id;
	newScore->myScore = inScore;
	if (studentScore == nullptr) {
		studentScore = newScore;
	}
	else {
		score* current = studentScore;
		while (current->next != nullptr) {
			current = current->next;
		}
		current->next = newScore;
	}
}

void Data::checkOutcome() {
	//the current pointer will move through the linked list to show the data stored in each node
	outcome* current = head;
	if (current == nullptr)
	{
		cout << "\nEmpty Outcomes list\n";
	}
	while (current != nullptr) {
		cout << "Outcome title:   " << current->title << endl;
		cout << "Outcome ID:   " << current->id << endl;
		cout << endl;
		current = current->next;
	}
}

void Data::checkStudent() {
	//the current pointer will move through the linked list to show the data stored in each node
	students* current = headStudent;
	if (current == nullptr)
	{
		cout << "\nEmpty Student list\n";
	}
	while (current != nullptr) {
		cout << "Student number:   " << current->num << endl;
		current->checkScore(current);
		cout << endl;
		current = current->next;
	}
}

void Data::students::checkScore(students* currentStudent) {
	//the current pointer will move through the linked list to show the data stored in each node
	score* current = currentStudent->studentScore;
	while (current != nullptr) {
		cout << "Outcome ID:   " << current->outcomeID << endl; 
		cout << "Score:   " << current->myScore << endl;
		cout << endl;
		current = current->next;
	}
}

void Data::readOutcomeFromStream() {
	double id;
	string title;
	for (outcome* current = head; current != nullptr; current = current->next) {
		id = current->id;
		title = current->title;
		//yourFunctionCall(id, title);
	}
}

void Data::readStudentFromStream() {
	int num;
	for (students* current = headStudent; current != nullptr; current = current->next) {
		num = current->num;
		//yourFunctionCall(num);
		current->readScoreFromStream(current);
	}
}

void Data::students::readScoreFromStream(students* currentStudent) {
	double myScore;
	double outcomeID;
	for (score* current = currentStudent->studentScore; current != nullptr; current = current->next) {
		current->myScore;
		current->outcomeID;
		//yourFunctionCall(myScore, outcomeID);
	}
}

Data::Data() {
	//Initializing the head of the outcome and student lists to nullptr
	head = nullptr;
	headStudent = nullptr;
}

Data::~Data() {
	//nodePtr is used to move the node used for deleting forward after the data from each node has been deleted.
	//listNode nodePtr
	outcome* outcomePtr = head;
	students* studentPtr = headStudent;
	/*
	*While delNode (The pointer I'll use for deleting) isn't a null pointer, this will continually run down the link list,
	*pulling the delNode to the nodePtr which is heading up the list until it gets to the nullptr at the end of the list.
	*/
	//cout << "Deleting all lists...\n";
	for (outcome* delNode = head; delNode != nullptr;) {
		outcomePtr = delNode->next;
		//cout << "Deleting node: 0x" << delNode;
		//cout << ", id: " << delNode->id;
		//cout << ", title:" << delNode->title;
		//cout << ", next: 0x" << delNode->next << endl;
		delNode->next = nullptr;
		delNode->id = NULL;
		delNode->title = "";
		delete delNode;
		delNode = outcomePtr;
	}
	for (students* delNode = headStudent; delNode != nullptr;) {
		studentPtr = delNode->next;
		//cout << "Deleting node: 0x" << delNode;
		//cout << ", num: " << delNode->num;
		//cout << ", score:";
		delNode->deleteScore(delNode->studentScore);
		//cout << ", next: 0x" << delNode->next << endl;
		delNode->next = nullptr;
		delNode->num = NULL;
		delete delNode;
		delNode = studentPtr;
	}
	head = nullptr;
	headStudent = nullptr;
	
}

void Data::students::deleteScore(score* studentScore) {
	for (score* delNode = studentScore; delNode != nullptr;) {
		studentScore = delNode->next;
		/*cout << "Deleting node: 0x" << delNode;
		cout << ", Outcome ID: " << delNode->myScore;
		cout << ", score:" << delNode->myScore;
		cout << ", next: 0x" << delNode->next << endl;*/
		delNode->next = nullptr;
		delNode->outcomeID = NULL;
		delNode->myScore = NULL;
		delete delNode;
		delNode = studentScore;
	}
}